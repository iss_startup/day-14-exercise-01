(function () {
    angular
        .module("DepartmentApp")
        .controller("SearchCtrl", SearchCtrl);

    SearchCtrl.$inject = ["$http"];

    function SearchCtrl($http) {
        var vm = this;

        vm.deptName = '';
        vm.result = null;
        vm.showManager = false;

        // Exposed functions
        vm.search = search;
        vm.searchForManager = searchForManager;

        // Initial calls
        init();


        // Function definitions
        function init() {
            $http
                .get("/api/departments")
                .then(function (response) {
                    vm.departments = response.data;
                })
                .catch(function (err) {
                    cosole.log("error " + err);
                });
        }

        function search() {
            vm.showManager = false;
            $http
                .get("/api/departments",
                    {
                        params: {
                            'dept_name': vm.deptName
                        }
                    }
                )
                .then(function (response) {
                    vm.departments = response.data;
                })
                .catch(function (err) {
                    console.info("Error " + err);
                });
        }

        function searchForManager() {
            vm.showManager = true;
            $http.get("/api/managers",
                {
                    params: {
                        'dept_name': vm.deptName
                    }
                })
                .then(function (response) {
                    vm.departments = response.data;
                })
                .catch(function (err) {
                    console.info("Error " + err);
                });
        }
    }

})();